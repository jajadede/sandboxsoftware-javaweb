/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.validator;

import com.jajadede.javaweb.entity.Inquiry;
import com.jajadede.javaweb.entity.Recommendation;
import java.util.Collection;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Jerry
 */
@Component
public class InquiryValidator implements Validator{

    public static final String EMAIL_PATTERN = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";

    @Override
    public boolean supports(Class<?> clazz) {
        return Inquiry.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Inquiry inquiry = (Inquiry) target;

        // common check        
        validateEmail(inquiry, errors);
        validateNotEmpty(inquiry, errors);
        
        // check against db
    }
    
    private void validateEmail(Inquiry inquiry, Errors errors) {
        if (inquiry.getEmail() != null && !inquiry.getEmail().isEmpty()
                && !Pattern.matches(EMAIL_PATTERN,inquiry.getEmail())) {
            errors.rejectValue(
                    "email",
                    "validator.InquiryValidator.email.message");
        }        
    }
    
    private void validateNotEmpty(Inquiry inquiry, Errors errors) {        
        if (StringUtils.isEmpty(inquiry.getInquiry()) || StringUtils.isEmpty(inquiry.getRecommendations())) {
            errors.rejectValue(null, "validator.InquiryValidator.requiredFields.message");            
            return;
        }
        
        Collection<Recommendation> recommendations = inquiry.getRecommendations();
        if (recommendations != null) {
            for (Recommendation rec : recommendations) {
                if (rec == null || (rec.isIsInUse() && StringUtils.isEmpty(rec.getRecommendation())) ){
                    errors.rejectValue(null, "validator.InquiryValidator.requiredFields.message");
                    break;
                }
            }
        }
    }    
}

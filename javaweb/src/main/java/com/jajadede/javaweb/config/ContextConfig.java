/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 * @author Jerry
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.jajadede.javaweb"})
public class ContextConfig{
    
    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] {"com.jajadede.javaweb.entity"});
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
    }
 
   @Bean
   public DataSource dataSource(){
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
      dataSource.setUrl("jdbc:mysql://localhost:3306/javaweb?useUnicode=yes&autoReconnect=true&useSSL=false");
      dataSource.setUsername( "javaweb" );
      dataSource.setPassword( "JavaWebPW900" );
      return dataSource;
   }
 
   private Properties hibernateProperties() {
       Properties properties = new Properties();
       // validate | update | create | create-drop
       properties.setProperty("hibernate.hbm2ddl.auto", "update");
       // MySQL8Dialect | MySQL5Dialect
       properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
       // show sql
       properties.setProperty("hibernate.show_sql", "false");
       // format sql
       properties.setProperty("hibernate.format_sql", "true");
       // use cache
       properties.setProperty("c3p0.timeout", "10");
        
       return properties;
   }
   
    @Bean
    public HibernateTransactionManager getTransactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }    
}

/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.dao;

import com.jajadede.javaweb.entity.Inquiry;
import com.jajadede.javaweb.entity.Recommendation;
import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Jerry
 */
@Transactional
@Repository ("inquiryRepository")
public class InquiryRepositoryImpl implements InquiryRepository {

    @Autowired
    private SessionFactory sessionFactory;
        
    @Override
    public Integer create(Inquiry inquiry) {
        Session session = sessionFactory.getCurrentSession();
        Integer id = (Integer) session.save(inquiry);
        Inquiry dbInquiry = session.get(Inquiry.class, id);
        if (inquiry.getRecommendations() != null) {
            inquiry.getRecommendations().forEach(r -> {
                r.setInquiry(dbInquiry);
                session.saveOrUpdate(r);
            });
        }
        return id;
    }

    @Override
    public List<Inquiry> readAllSortedByLastName() {
        Session session = sessionFactory.getCurrentSession();
        Query<Inquiry> query = session.createQuery(""
                + "select distinct i "
                + "from Inquiry i "
                + "left join fetch i.recommendations "
                + "where i.firstName is not null "
                + "or i.lastName is not null "
                + "or i.email is not null "
                + "or i.inquiry is not null "
                + "or i.created is not null "
                + "order by i.lastName "
//                + "desc "
        );
        return query.list();
    }

    @Override
    public void update(Inquiry inquiry) {
//        inquiry.print("before update");
        Session session = sessionFactory.getCurrentSession();
        session.merge(inquiry);
        if (inquiry.getRecommendations() != null) {
            inquiry.getRecommendations().forEach(r -> {
                r.setInquiry(inquiry);
                session.merge(r);
            });
        }        
    }

    @Override
    public Integer addRecommendation(Recommendation recommendation) {
        Integer id = (Integer) sessionFactory.getCurrentSession().save(recommendation);
        return id;  
    }

    @Override
    public void removeRecommendation(Integer recommendationId) {
        Session session = sessionFactory.getCurrentSession();
        Recommendation r = session.get(Recommendation.class, recommendationId);
        session.delete(r);
    }

    @Override
    public Inquiry findById(Integer inquiryId) {
        return sessionFactory.getCurrentSession().get(Inquiry.class, inquiryId);
    }

    @Override
    public Recommendation findRecommendationById(Integer recommendationId) {
        return sessionFactory.getCurrentSession().get(Recommendation.class, recommendationId);
    }

    @Override
    public int removeBlankInquires() {
        Session session = sessionFactory.getCurrentSession();        
        Query<Inquiry> query = session.createQuery(""
                + "select i "
                + "from Inquiry i "
                + "where i.firstName is null "
                + "and i.lastName is null "
                + "and i.email is null "
                + "and i.inquiry is null "
                + "and i.created is null"
        );
        List<Inquiry> result = query.list();        
        for(Inquiry i : result) session.delete(i);

        return result.size();
    }
}

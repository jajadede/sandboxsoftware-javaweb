/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jerry
 */
@Entity
@Table(name = "inquiries")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inquiry.findAll", query = "SELECT i FROM Inquiry i")
    , @NamedQuery(name = "Inquiry.findById", query = "SELECT i FROM Inquiry i WHERE i.id = :id")
    , @NamedQuery(name = "Inquiry.findByFirstName", query = "SELECT i FROM Inquiry i WHERE i.firstName = :firstName")
    , @NamedQuery(name = "Inquiry.findByLastName", query = "SELECT i FROM Inquiry i WHERE i.lastName = :lastName")
    , @NamedQuery(name = "Inquiry.findByEmail", query = "SELECT i FROM Inquiry i WHERE i.email = :email")
    , @NamedQuery(name = "Inquiry.findByCreated", query = "SELECT i FROM Inquiry i WHERE i.created = :created")})
public class Inquiry implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final int CHAR_NUM_COMPARE_TO = 150;    
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Size(max = 255)
    @Column(name = "first_name")
    private String firstName;
    
    @Size(max = 255)
    @Column(name = "last_name")
    private String lastName;
    
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    
    @Basic(optional = false)
    @Lob
    @Size(max = 65535)
    @Column(name = "inquiry")
    private String inquiry;
    
    @Basic(optional = false)
    @Column(name = "created")
    @Temporal(TemporalType.DATE)
    private Date created;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "inquiry")
    private List<Recommendation> recommendations;

    // fields not in database
    @Transient
    private int inquiryCharCount;
    @Transient
    private String comparisionResult;
    
    public Inquiry() {
    }

    public Inquiry(Integer id) {        
        this.id = id;
    }

    public Inquiry(Integer id, String inquiry, Date created) {
        this.id = id;
        this.inquiry = inquiry;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInquiry() {
        return inquiry;
    }

    public void setInquiry(String inquiry) {
        this.inquiry = inquiry;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @XmlTransient
    public List<Recommendation> getRecommendations() {
        return recommendations;
    }

    public void setRecommendations(List<Recommendation> recommendations) {
        this.recommendations = recommendations;
    }

    public int getInquiryCharCount() {
        this.updateInquiryCharCount();
        return inquiryCharCount;
    }

    public void updateInquiryCharCount() {
        if (inquiry == null) this.inquiryCharCount = 0;
        else this.inquiryCharCount = inquiry.length();
    }

    public String getComparisionResult() {
        updateComparisionResult();
        return comparisionResult;
    }

    public void updateComparisionResult() {
        int difference = getInquiryCharCount() - CHAR_NUM_COMPARE_TO;
        
        if (difference < 0) this.comparisionResult = "<";
        else if (difference == 0) this.comparisionResult = "=";
        else this.comparisionResult = ">";
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        hash = 83 * hash + Objects.hashCode(this.firstName);
        hash = 83 * hash + Objects.hashCode(this.lastName);
        hash = 83 * hash + Objects.hashCode(this.email);
        hash = 83 * hash + Objects.hashCode(this.inquiry);
        hash = 83 * hash + Objects.hashCode(this.created);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Inquiry other = (Inquiry) obj;
        if (!Objects.equals(this.firstName, other.firstName)) {
            return false;
        }
        if (!Objects.equals(this.lastName, other.lastName)) {
            return false;
        }
        if (!Objects.equals(this.email, other.email)) {
            return false;
        }
        if (!Objects.equals(this.inquiry, other.inquiry)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.created, other.created)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.jajadede.javaweb.entity.Inquiry[ id=" + id + " ]";
    }

    public void addRecommendation(Recommendation recommendation) {
        if (recommendations==null) recommendations = new ArrayList<Recommendation>();
        recommendations.add(recommendation);
        recommendation.setInquiry(this);
    }
    
 
    public void print(String tag){
        System.out.println("******************" + tag);
        System.out.println("id: '" + this.getId() + "'");
        System.out.println("firstName: '" + this.getFirstName() + "'");
        System.out.println("lastName: '" + this.getLastName() + "'");
        System.out.println("inquiry: '" + this.getInquiry() + "'");
        for(Recommendation r : this.getRecommendations()){
            System.out.println("recommendation.id: '" + r.getId() + "'");
            System.out.println("recommendation.recommendation: '" + r.getRecommendation() + "'");
            System.out.println("recommendation.inquiry: '" + r.getInquiry() + "'");
            System.out.println("recommendation.isInUse: '" + r.isIsInUse() + "'");
        }
    }    
      
}

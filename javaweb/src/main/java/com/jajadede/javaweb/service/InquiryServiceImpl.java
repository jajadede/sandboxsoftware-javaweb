/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.service;

import com.jajadede.javaweb.dao.InquiryRepository;
import com.jajadede.javaweb.entity.Inquiry;
import com.jajadede.javaweb.entity.Recommendation;
import java.util.Calendar;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Jerry
 */
@Service("inquiryService")
public class InquiryServiceImpl implements InquiryService {

    @Autowired
    private InquiryRepository inquiryRepository;
    
    @Override
    @Transactional
    public Inquiry createBlankInquiry() {
        Inquiry inquiry = new Inquiry();
        Integer inquiryId = inquiryRepository.create(inquiry);
        return inquiryRepository.findById(inquiryId);
    }

    @Override
    @Transactional
    public Recommendation createBlankRecommendation(Integer inquiryId) {
        Recommendation temp = new Recommendation();
        temp.setInquiry(inquiryRepository.findById(inquiryId));
        Integer recommendationId = inquiryRepository.addRecommendation(temp);
        return inquiryRepository.findRecommendationById(recommendationId);
    }

    @Override
    @Transactional
    public void removeRecommendation(Integer recommendationId) {
        inquiryRepository.removeRecommendation(recommendationId);
    }

    @Override
    @Transactional
    public void update(Inquiry inquiry) {
        inquiry.setCreated(Calendar.getInstance().getTime());
        inquiryRepository.update(inquiry);
    }

    @Override
    @Transactional
    public List<Inquiry> findAllSortedByLastName() {
        return inquiryRepository.readAllSortedByLastName();
    }

    @Override
    @Transactional
    public int removeBlankInquires() {
        return inquiryRepository.removeBlankInquires();
    }
    
}

/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.service;

import com.jajadede.javaweb.entity.Inquiry;
import com.jajadede.javaweb.entity.Recommendation;
import java.util.List;

/**
 *
 * @author Jerry
 */
public interface InquiryService {
    
    Inquiry createBlankInquiry();
    
    Recommendation createBlankRecommendation(Integer inquiryId);
    
    void removeRecommendation(Integer recommendationId);
    
    void update(Inquiry inquiry);
    
    List<Inquiry> findAllSortedByLastName();
    
    int removeBlankInquires();
}

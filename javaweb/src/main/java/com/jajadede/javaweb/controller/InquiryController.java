/*
 * Copyright (C) 2019 Jerry
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.jajadede.javaweb.controller;

import com.jajadede.javaweb.entity.Inquiry;
import com.jajadede.javaweb.entity.Recommendation;
import com.jajadede.javaweb.validator.InquiryValidator;
import com.jajadede.javaweb.service.InquiryService;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author Jerry
 */
@Controller
@RequestMapping("/inquiry")
@SessionAttributes("inquiry")
public class InquiryController {
    private static final int MAX_SIZE_NUM_RECOMMENDATION = 10;
    
    @Autowired
    private InquiryService inquiryService;
    
    @Autowired
    private InquiryValidator inquiryValidator;
    
    @GetMapping("/list")
    public String listAllInquiresSortedByLastName(Model model) {
        List<Inquiry> inqs = inquiryService.findAllSortedByLastName();
        model.addAttribute("inquiries", inqs);
        return "submissions";
    } 
    
    @GetMapping("/form")
    public String showForm(Model model) {
        Inquiry inquiry = inquiryService.createBlankInquiry();
        for (int i=1; i<= MAX_SIZE_NUM_RECOMMENDATION; i++) inquiry.addRecommendation(inquiryService.createBlankRecommendation(inquiry.getId()));
        inquiry.getRecommendations().get(0).setIsInUse(true); // only show the first one
        model.addAttribute("inquiry", inquiry);
//        inquiry.print("/inquiry/form");
        return "inquiry";
    }    
    
    @PostMapping("/form")
    public String inquiryFormProcess(            
            @ModelAttribute("inquiry") @Valid Inquiry inquiry,
            BindingResult result,
            Model model,
            SessionStatus status) {
//        inquiry.print("/form post [before process]");
        if (result.hasErrors()) {
            model.addAttribute("inquiry", inquiry);
            return "inquiry";
        }
        try {
            List<Recommendation> toRemoveFromDetachedCollection = new ArrayList<Recommendation>();
            for(Recommendation r : inquiry.getRecommendations()){
                if (!r.isIsInUse()) {                    
                    inquiryService.removeRecommendation(r.getId()); 
//                    System.out.println("********* recommendation removed from db: " + r);
                    toRemoveFromDetachedCollection.add(r);
                }
            }
            for(Recommendation r : toRemoveFromDetachedCollection){                 
                inquiry.getRecommendations().remove(r);
//                System.out.println("********* recommendation removed from detached: " + r);
            }            
//            inquiry.print("/form post [after removeRecommendations]");
            inquiryService.update(inquiry);
//            inquiry.print("/form post [after update(inquiry)]");
        }
        catch (Exception e) {
            result.addError(new ObjectError("", e.getMessage()));
            model.addAttribute("inquiry", inquiry);
            return "inquiry";            
        }
//        status.setComplete(); // end the spring session
        return "redirect:/inquiry/list";
    }
    
    @GetMapping("/addRecommendation")
    public String addRecommendation(
            @RequestParam("inquiryId") Integer inquiryId, 
            Model model, 
            HttpSession session,
            HttpServletRequest request) {

        Inquiry inquiry = (Inquiry) session.getAttribute("inquiry");        
        if (inquiry != null) {
//            inquiry.print("/addRecommendation");
            inquiry.addRecommendation(inquiryService.createBlankRecommendation(inquiryId));
            model.addAttribute("inquiry", inquiry);
//            session.setAttribute("inquiry", inquiry);
        }        
        return "inquiry";
    }
    
    @GetMapping("/cleanup")
    public String cleanup(Model model) {
        model.addAttribute("numRemovedRecords", inquiryService.removeBlankInquires());
        return "cleanup-result";
    }
    
    @InitBinder
    public void initialseBinder(WebDataBinder binder) {
        binder.addValidators(inquiryValidator);
        binder.setAllowedFields( 
//                "lastName", "firstName", "email", "inquiry", "recommendations"
                "*"
        );
    }    
}

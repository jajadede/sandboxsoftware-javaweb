<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap 3 CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
        <title>Cleanup Result</title>
    </head>
    <body>
        <div class="container">
            <center>

                <h1>Cleanup Result</h1>
                <h2>${numRemovedRecords} record(s) was removed.</h2>
                <p>
                    Server Version: <%= application.getServerInfo()%><br>
                    Servlet Version: <%= application.getMajorVersion()%>.<%= application.getMinorVersion()%> <br>            
                </p></center>
        </div>
    </body>
</html>

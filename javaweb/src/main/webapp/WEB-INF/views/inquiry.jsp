<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${req.scheme}://${req.serverName}:${req.serverPort}${req.contextPath}" />

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap 3 CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
        <title>Inquiry Form Page</title>
    </head>
    <body>
        <div class="container">
            <center><h1>Inquiry Form</h1></center>
                <form:form method="POST" modelAttribute="inquiry" class="form-horizontal" action="${baseURL}/inquiry/form">
                    <form:errors path="*" cssClass="alert alert-danger" element="div"/>
                    <form:hidden path="id" />
                <div class="form-group ">
                    <label class ="col-sm-5 control-label" for="firstName"><spring:message code="views.inquiry.form.label.firstName"/></label>
                    <div class ="col-sm-4">
                        <form:input type="text" class="form-control" id="firstName" placeholder="${inquiry.firstName}" name="firstName" path="firstName"/>
                    </div>
                </div>    
                <div class="form-group ">
                    <label class ="col-sm-5 control-label" for="lastName"><spring:message code="views.inquiry.form.label.lastName"/></label>
                    <div class ="col-sm-4">
                        <form:input type="text" class="form-control" id="lastName" placeholder="${inquiry.lastName}" name="lastName" path="lastName"/>
                    </div>
                </div> 
                <div class="form-group ">
                    <label class ="col-sm-5 control-label" for="email"><spring:message code="views.inquiry.form.label.email"/></label>
                    <div class ="col-sm-4">
                        <form:input type="text" class="form-control" id="email" placeholder="${inquiry.email}" name="email" path="email"/>
                    </div>
                </div>   
                <div class="form-group ">
                    <label class ="col-sm-5 control-label" for="inquiryInput"><spring:message code="views.inquiry.form.label.inquiry"/></label>
                    <div class ="col-sm-4">
                        <form:input type="text" class="form-control" id="inquiryInput" placeholder="${inquiry.inquiry}" name="inquiry" path="inquiry"/>
                    </div>
                </div>  
                <c:forEach items="${inquiry.recommendations}" var="recommendationVar" varStatus="idx" >
                    <form:hidden path="recommendations[${idx.index}].id" />
                    <form:hidden path="recommendations[${idx.index}].isInUse" />
                    <div class="form-group " <c:if test="${!recommendationVar.isInUse}">style="display:none;"</c:if> index="${idx.index}">
                        <label class ="col-sm-5 control-label" for="recommendationInput${recommendationVar.id}"><spring:message code="views.inquiry.form.label.recommendation"/></label>
                        <div class ="col-sm-4">
                            <form:input type="text" class="form-control" id="recommendationInput${recommendationVar.id}" placeholder="${recommendationVar.recommendation}" name="recommendation${recommendationVar.id}" path="recommendations[${idx.index}].recommendation"/>
                        </div>
                    </div>
                </c:forEach>

                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-4">
                        <button type="button" id="addRecommendationButton" class="btn btn-primary" onclick="addRecommendation()" ><spring:message code="views.inquiry.form.label.recommendation.add"/></button>
                    </div>
                </div>            
                <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-4">
                        <button type="submit" class="btn btn-primary"><spring:message code="views.inquiry.form.label.submit"/></button>
                    </div>
                </div>            
            </form:form>
        </div>
    </body>

    <script>
        $(document).ready(function () {
            var recommendations = $("[id^='recommendationInput']").parent().parent();
            var size = recommendations.length;
            var hidden = recommendations.filter(function () {
                return this.style.display === "none"
            }).length;
//            alert("hidden/size="+hidden+"/"+size);
            if (hidden === 0)
                $("#addRecommendationButton").attr("disabled", "disabled");
        });
        function addRecommendation() {
            var recommendations = $("[id^='recommendationInput']").parent().parent();
            var size = recommendations.length;
            var countDisplay = 0;
            var added = false;
            recommendations.each(function () {
                if (this.style.display === "none") {
                    if (!added) {
                        this.style.display = "block";
                        added = true;
                        var isInUseInput_id = "#recommendations" + $(this).attr('index') + "\\.isInUse";
                        $(isInUseInput_id).attr("value", "true");
                        countDisplay++;
                    }
                } else {
                    countDisplay++;
                }

            });
//            alert("countDisplay/size="+countDisplay+"/"+size);
            if (countDisplay === size) {
                $("#addRecommendationButton").attr("disabled", "disabled");
            }
        }
    </script>    
</html>

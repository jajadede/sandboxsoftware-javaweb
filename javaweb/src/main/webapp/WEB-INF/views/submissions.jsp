<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap 3 CDN -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>  
        <link rel="stylesheet" href="<c:url value="/css/style.css" />">
        <title>Submissions Page</title>
    </head>
    <body>
        <div class="container">
            <center><h1>All Submissions</h1></center>
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th><spring:message code="views.submissions.table.head.lastName"/></th>
                            <th><spring:message code="views.submissions.table.head.firstName"/></th>
                            <th><spring:message code="views.submissions.table.head.email"/></th>
                            <th><spring:message code="views.submissions.table.head.inquiry"/></th>
                            <th><spring:message code="views.submissions.table.head.charCount"/></th>
                            <th><spring:message code="views.submissions.table.head.comparison"/></th>
                            <th><spring:message code="views.submissions.table.head.dateOfSubmission"/></th>
                            <th><spring:message code="views.submissions.table.head.recommendations"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${inquiries}" var="inquiry">
                            <tr>
                                <td>${inquiry.lastName}</td>
                                <td>${inquiry.firstName}</td>
                                <td>${inquiry.email}</td>
                                <td>${inquiry.inquiry}</td>
                                <td>${inquiry.inquiryCharCount}</td>
                                <td>${inquiry.comparisionResult}</td>
                                <td>${inquiry.created}</td>
                                <td>
                                    <c:if test="${inquiry.recommendations.size()>0}">
                                        <ul>
                                            <c:forEach items="${inquiry.recommendations}" var="recommendationVar" varStatus="idx" >
                                                <li>${recommendationVar.recommendation}</li>
                                            </c:forEach>
                                        </ul>
                                    </c:if>
                                </td>
                            </tr>  
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>

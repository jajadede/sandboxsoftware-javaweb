# sandboxsoftware-javaweb

Practical exercise

# Tools and technologies used:
Spring MVC - 5.1.0RELEASE
Hibernate - 5.3.5.FINAL
JDK - 1.8
Maven - 3.5.4
Apache Tomcat - 9.0.11 (dev) | 7.0.37 (executable)
IDE - NetBeans IDE 8.2
JSTL - 1.2
Servlet - 4.0
MySQL - 8.0.12

# To compile the code to executable jar
mvn clean install tomcat7:exec-war-only


# To run the executable jar file
java -jar javaweb.jar
To stop it, just press Ctrl+C in the command window.
You may also double click the javaweb.jar file to start, then you need to find a way to kill its java.exe process.


# To run the executable at a different port (httpPort is case sensitive), for example: port 80
java -jar javaweb.jar -httpPort 80


# In case it doesn't connect to your MySQL database, you may need to 
change the config file at com.jajadede.javaweb.config.ContextConfig.java
set hibernate.dialect to MySQL5Dialect in the method hibernateProperties()


# To visit the web app, use the following address: 
localhost:8080/javaweb


# To access the app directly:
localhost:8080/javaweb/inquiry/form


# To access the submissions page directly:
localhost:8080/javaweb/inquiry/list


# To remove blanck inquiries from the database:
localhost:8080/javaweb/inquiry/cleanup


# Known issue:
Unsaved inquiries and their recommendations are left blank in database. Use the address above to do the cleanup manually.

In the first implementaion, inquiry is kept in session, each time adding a new recommmendation will clean up the whole form. To solve this issue, a max number of recommendations for each inquiry is introduced. The number is now set to 10.


# Need to know more? 
Check other documents under doc fold
